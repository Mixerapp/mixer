package alessia.com.mixer.Utils;

import android.content.Context;

/**
 * Created by Alessia on 09/06/2017.
 */

public class Utils {

    private static final String SHARED = "shared";
    private static final String KEY_USER = "key_user";
    private static final String TYPE_LOG = "type_log";
    public static void setUserId(Context context, String userId) {
        context.getSharedPreferences(SHARED, Context.MODE_PRIVATE).edit().putString(KEY_USER, userId).apply();
    }

    public static String getUserId(Context context) {
        return context.getSharedPreferences(SHARED, Context.MODE_PRIVATE).getString(KEY_USER, null);
    }

    public static void setTypeLog(Context context, String typeLog){
        context.getSharedPreferences(SHARED,Context.MODE_PRIVATE).edit().putString(TYPE_LOG,typeLog).apply();
    }

    public static String getTypeLog(Context context){
        return context.getSharedPreferences(SHARED,Context.MODE_PRIVATE).getString(TYPE_LOG, null);
    }


}
