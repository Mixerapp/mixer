package alessia.com.mixer.Fragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import alessia.com.mixer.Activity.ItemDetailsSmoothies;
import alessia.com.mixer.Model.Smoothies;
import alessia.com.mixer.R;

/**
 * Created by Alessia on 09/05/2017.
 */

public class SmoothiesListFragment extends Fragment {
    private GridView listview;
    private DatabaseReference mysmoothies;
    private FirebaseDatabase database;
    private List<Smoothies> smoothies = new ArrayList<>();
    private SmoothiesAdapter adapter;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle saveInstanceState){

        View rootView = inflater.inflate(R.layout.shakes_list_fragment,container,false);
        listview = (GridView) rootView.findViewById(R.id.shakes_list);

        database = FirebaseDatabase.getInstance();
        mysmoothies = database.getReference("smoothies");

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Smoothies selectSmoothies = (Smoothies) parent.getAdapter().getItem(position);
                Intent i = new Intent(getContext(), ItemDetailsSmoothies.class);
                i.putExtra("id", selectSmoothies);
                startActivity(i);
            }
        });

        ValueEventListener postListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot singleSmoothies : dataSnapshot.getChildren()) {
                    Smoothies smoothie = new Smoothies();
                    String s;
                    List<String> ing = new ArrayList<>();
                    smoothie.setLink(singleSmoothies.child("image").getValue(String.class));
                    smoothie.setName(singleSmoothies.child("name").getValue(String.class));
                    s =  singleSmoothies.child("ingredients").getValue(String.class);
                    for (String split:s.split("\\|")){
                        if(split.trim()!=null && !split.isEmpty()){
                            ing.add(split);
                        }
                    }
                    smoothie.setIngredients(ing.toString());
                    smoothies.add(smoothie);
                }
                if (adapter != null) {
                    adapter.swap(new ArrayList<>(smoothies));
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };
        mysmoothies.addListenerForSingleValueEvent(postListener);
        adapter = new SmoothiesAdapter(this.getContext());
        adapter.setData(smoothies);
        this.listview.setAdapter(adapter);
        return rootView;
    }

    private class SmoothiesAdapter extends BaseAdapter {

        private LayoutInflater inflater;
        private List<Smoothies> smoothies;

        public SmoothiesAdapter(final Context context){
            this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        public void setData(final List<Smoothies> smoothies){
            this.smoothies = smoothies;
        }
        @Override
        public int getCount() {
            if(this.smoothies == null){
                return 0;
            }
            return this.smoothies.size();
        }

        @Override
        public Object getItem(int position) {
            if(this.smoothies == null || this.smoothies.get(position) == null){
                return null;
            }
            return this.smoothies.get(position);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent){
            SmoothiesListFragment.ViewHolder viewHolder;
            if(convertView == null){
                convertView = this.inflater.inflate(R.layout.shake_list_item, parent, false);
                viewHolder = new ViewHolder();
                viewHolder.initializeComponents(convertView);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            if(URLUtil.isValidUrl(smoothies.get(position).getLink())){
                //Picasso
                Picasso.with(SmoothiesListFragment.this.getContext()).setLoggingEnabled(true);
                Picasso.with(SmoothiesListFragment.this.getContext()).load(smoothies.get(position).getLink())
                        .resize(400,400).into(viewHolder.img);
            }else{

                byte[] decodedString = Base64.decode(smoothies.get(position).getLink(),Base64.DEFAULT);
                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString,0,decodedString.length);

                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                decodedByte.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                String path = MediaStore.Images.Media.insertImage(SmoothiesListFragment.this.getContext().getContentResolver(),decodedByte, "title", null);

                Picasso.with(SmoothiesListFragment.this.getContext()).load(Uri.parse(path))
                        .resize(400,400).into(viewHolder.img);
            }

            //Picasso
            viewHolder.stringImg.setText(smoothies.get(position).getName());
            viewHolder.stringIngre.append(smoothies.get(position).getIngredients());
            return convertView;
        }

        public void swap(List<Smoothies> newSmoothies) {
            this.smoothies.clear();
            this.smoothies.addAll(newSmoothies);
            notifyDataSetChanged();
        }
    }

    private static class ViewHolder{
        ImageView img;
        TextView stringImg;
        TextView stringIngre;
        public void initializeComponents(final View view){
            this.img = (ImageView) view.findViewById(R.id.img_list);
            this.stringImg = (TextView) view.findViewById(R.id.title_shake);
            this.stringIngre = (TextView) view.findViewById(R.id.ingredients);
            stringIngre.setSelected(true);

        }
    }
}
