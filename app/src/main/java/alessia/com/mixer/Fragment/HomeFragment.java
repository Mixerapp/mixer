package alessia.com.mixer.Fragment;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import alessia.com.mixer.Activity.HomeActivity;
import alessia.com.mixer.Model.News;
import alessia.com.mixer.R;
import alessia.com.mixer.databinding.HomeBinding;
import alessia.com.mixer.databinding.HomeFragmentBinding;
import alessia.com.mixer.databinding.NewsItemBinding;
import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;

/**
 * Created by Alessia on 08/04/2017.
 */

public class HomeFragment extends Fragment {
    private FirebaseDatabase database;
    private DatabaseReference news;
    private List<News> newsArrayList = new ArrayList<>();
    private NewsAdapter adapter;
    private HomeFragmentBinding binding;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle saveInstanceState) {

        this.binding= DataBindingUtil.inflate(inflater,R.layout.home_fragment,container,false);
        View rootView = this.binding.getRoot();

        /*scarico dati firebase*/
        database = FirebaseDatabase.getInstance();
        news = database.getReference("news");

        ValueEventListener postListener = new ValueEventListener() {
            Realm realm = Realm.getDefaultInstance();
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot singleNews : dataSnapshot.getChildren()) {
                    /*dati dentro realm*/

                    /*realm.beginTransaction();*/

                    /*News news = realm.createObject(News.class,singleNews.child("id").getValue(Long.class));
                    //news.setId(singleNews.child("id").getValue(Long.class));
                    news.setLink(singleNews.child("image").getValue(String.class));
                    news.setTitle(singleNews.child("title").getValue(String.class));
                    realm.copyToRealmOrUpdate(news);
                    realm.commitTransaction();*/
                    //newsArrayList.add(news);

                    final News news = new News();
                    news.setId(singleNews.child("id").getValue(Long.class));
                    news.setLink(singleNews.child("image").getValue(String.class));
                    news.setTitle(singleNews.child("title").getValue(String.class));
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            // This will create a new object in Realm or throw an exception if the
                            // object already exists (same primary key)
                            // realm.copyToRealm(obj);

                            // This will update an existing object with the same primary key
                            // or create a new object if an object with no primary key = 42
                            realm.copyToRealmOrUpdate(news);
                        }
                    });

                }
               /* if (adapter != null) {
                    adapter.swap(new ArrayList<>(newsArrayList));
                }*/
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };news.addListenerForSingleValueEvent(postListener);

        Realm realm = Realm.getDefaultInstance();
        RealmQuery<News> query = realm.where(News.class);
        RealmResults<News> result1 = query.findAll();

        Log.d("listanews", ""+result1);
        adapter = new NewsAdapter(this.getContext());
        adapter.setData(result1);
        this.binding.listNews.setAdapter(adapter);
        return rootView;
    }


    private class NewsAdapter extends BaseAdapter {

        private LayoutInflater inflater;
        private List<News> listNews;

        public NewsAdapter(final Context context){
            this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        @Override
        public int getCount() {
            if(this.listNews == null){
                return 0;
            }
            return this.listNews.size();
        }

        public void setData(final List<News> news){
            this.listNews = news;
        }

        @Override
        public Object getItem(int position) {
            if(this.listNews == null || this.listNews.get(position) == null){
                return null;
            }
            return this.listNews.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            ViewHolder viewHolder;
            if(convertView == null){
                convertView = this.inflater.inflate(R.layout.news_item, parent, false);
                viewHolder = new ViewHolder();
                viewHolder.initializeComponents(convertView);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            Picasso.with(HomeFragment.this.getContext()).setLoggingEnabled(true);
            Picasso.with(HomeFragment.this.getContext()).load(listNews.get(position).getLink())
                    .resize(400,400).into(viewHolder.img);
            viewHolder.title.setText(listNews.get(position).getTitle());
            return convertView;
        }

       /* public void swap(List<News> newNews) {
            this.listNews.clear();
            this.listNews.addAll(newNews);
            notifyDataSetChanged();
        }*/
    }

    private static class ViewHolder{
        ImageView img;
        TextView title;
        public void initializeComponents(final View view){
            this.img = (ImageView) view.findViewById(R.id.img_news);
            this.title = (TextView) view.findViewById(R.id.text_news);
        }
    }
}