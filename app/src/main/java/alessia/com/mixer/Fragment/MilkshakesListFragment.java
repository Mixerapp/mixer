package alessia.com.mixer.Fragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import alessia.com.mixer.Activity.ItemDetailsMilkShakes;
import alessia.com.mixer.Model.Milkshakes;
import alessia.com.mixer.R;
import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;

/**
 * Created by Alessia on 09/05/2017.
 */

public class MilkshakesListFragment extends Fragment {
    private GridView listview;
    private DatabaseReference mymilk;
    private FirebaseDatabase database;

    private MilkAdapter adapter;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle saveInstanceState){
        super.onCreate(saveInstanceState);
        View rootView = inflater.inflate(R.layout.shakes_list_fragment,container,false);
        listview = (GridView) rootView.findViewById(R.id.shakes_list);


        //ItemDetails
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Milkshakes selectMilk = (Milkshakes) parent.getAdapter().getItem(position);
                Intent i = new Intent(getActivity(), ItemDetailsMilkShakes.class);
                i.putExtra("id", selectMilk.getId());
                startActivity(i);
                //startActivity(i);
            }
        });

        database = FirebaseDatabase.getInstance();

        mymilk = database.getReference("milkshakes");


        ValueEventListener postListener = new ValueEventListener() {
            Realm realm = Realm.getDefaultInstance();

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot singleMilk : dataSnapshot.getChildren()) {
                    final Milkshakes milk = new Milkshakes();
                    String s;
                    List<String> ing = new ArrayList<>();
                    Log.d("singlemilk","ciao"+singleMilk.getKey());
                    milk.setIdName(singleMilk.getKey());
                    milk.setLink(singleMilk.child("link").getValue(String.class));
                    milk.setImage(singleMilk.child("image").getValue(String.class));
                    milk.setName(singleMilk.child("name").getValue(String.class));
                    milk.setId(singleMilk.getKey());
                    milk.setPreparing(singleMilk.child("preparing").getValue(String.class));
                    s =  singleMilk.child("ingredients").getValue(String.class);
                    for (String split:s.split("\\|")){
                        if(split.trim()!=null && !split.isEmpty()){
                            ing.add(split);
                        }
                    }
                    milk.setIngredients(ing.toString());
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            realm.copyToRealmOrUpdate(milk);
                        }
                    });
                    /*milkshakes.add(milk);*/
                }
                prova();
               /* if (adapter != null) {
                    adapter.swap(new ArrayList<>(milkshakes));
                }*/
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };
        mymilk.addListenerForSingleValueEvent(postListener);

        /*Realm realm = Realm.getDefaultInstance();
        RealmQuery<Milkshakes> query = realm.where(Milkshakes.class);
        RealmResults<Milkshakes> result1 = query.findAll();*/

        adapter = new MilkAdapter(this.getContext());
        this.listview.setAdapter(adapter);
        prova();
        /*adapter.setData(result1);
        this.listview.setAdapter(adapter);*/
        return rootView;
    }


    private void prova(){

        Realm realm = Realm.getDefaultInstance();
        RealmQuery<Milkshakes> query = realm.where(Milkshakes.class);
        RealmResults<Milkshakes> result1 = query.findAll();
        adapter.swap(new ArrayList<>(result1));
    }



    private class MilkAdapter extends BaseAdapter {

        private LayoutInflater inflater;
        private List<Milkshakes> milkshakes;

        public MilkAdapter(final Context context){
            this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            milkshakes = new ArrayList<>();
        }

        public void setData(final List<Milkshakes> milkshakes){
            this.milkshakes = milkshakes;
        }
        @Override
        public int getCount() {
            if(this.milkshakes== null){
                return 0;
            }
            return this.milkshakes.size();
        }

        @Override
        public Object getItem(int position) {
            if(this.milkshakes == null || this.milkshakes.get(position) == null){
                return null;
            }
            return this.milkshakes.get(position);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent){
            ViewHolder viewHolder;
            if(convertView == null){
                convertView = this.inflater.inflate(R.layout.shake_list_item, parent, false);
                viewHolder = new ViewHolder();
                viewHolder.initializeComponents(convertView);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            if(URLUtil.isValidUrl(milkshakes.get(position).getImage())){
                //Picasso
                Picasso.with(MilkshakesListFragment.this.getContext()).setLoggingEnabled(true);
                Picasso.with(MilkshakesListFragment.this.getContext()).load(milkshakes.get(position).getImage())
                        .resize(400,400).into(viewHolder.img);
            }else{

                byte[] decodedString = Base64.decode(milkshakes.get(position).getImage(),Base64.DEFAULT);
                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString,0,decodedString.length);

                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                decodedByte.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                String path = MediaStore.Images.Media.insertImage(MilkshakesListFragment.this.getContext().getContentResolver(),decodedByte, "title", null);

                Picasso.with(MilkshakesListFragment.this.getContext()).load(Uri.parse(path))
                        .resize(400,400).into(viewHolder.img);
            }

            viewHolder.stringImg.setText(milkshakes.get(position).getName());
            viewHolder.stringIngre.append(milkshakes.get(position).getIngredients());
            return convertView;
        }

        public void swap(List<Milkshakes> newMilk) {
            this.milkshakes.clear();
            this.milkshakes.addAll(newMilk);
            notifyDataSetChanged();
        }
    }

    private static class ViewHolder{
        ImageView img;
        TextView stringImg;
        TextView stringIngre;
        public void initializeComponents(final View view){
            this.img = (ImageView) view.findViewById(R.id.img_list);
            this.stringImg =  (TextView) view.findViewById(R.id.title_shake);
            this.stringIngre = (TextView) view.findViewById(R.id.ingredients);
            stringIngre.setSelected(true);

        }
    }
}
