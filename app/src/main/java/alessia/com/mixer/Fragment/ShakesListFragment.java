package alessia.com.mixer.Fragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import alessia.com.mixer.Activity.ItemDetailsShakes;
import alessia.com.mixer.Model.Shake;
import alessia.com.mixer.R;
import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;

import static alessia.com.mixer.R.id.shakes;
import static alessia.com.mixer.R.id.toolbar;


/**
 * Created by Alessia on 24/04/2017.
 */

public class ShakesListFragment extends Fragment{

    private GridView listview;
    private DatabaseReference myshakes;
    private FirebaseDatabase database;

    private ShakesAdapter adapter;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle saveInstanceState){
        super.onCreate(saveInstanceState);
        View rootView = inflater.inflate(R.layout.shakes_list_fragment,container,false);
        listview = (GridView) rootView.findViewById(R.id.shakes_list);


       /* TextView title = (TextView) rootView.findViewById(R.id.title);
        title.setText("SHAKES");*/
        //ItemDetails
       /*listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Shake selectShake = (Shake) parent.getAdapter().getItem(position);
                Intent i = new Intent(getContext(), ItemDetailsShakes.class);
                i.putExtra("id", selectShake);
                startActivity(i);
            }
        });*/

        //ItemDetails
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Shake selectShake = (Shake) parent.getAdapter().getItem(position);
                Intent i = new Intent(getActivity(), ItemDetailsShakes.class);
                i.putExtra("id", selectShake.getId());
                startActivity(i);
                //startActivity(i);
            }
        });

        database = FirebaseDatabase.getInstance();

        myshakes = database.getReference("shakes");

        ValueEventListener postListener = new ValueEventListener() {
            Realm realm = Realm.getDefaultInstance();
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot singleShake : dataSnapshot.getChildren()) {
                    final Shake shake = new Shake();
                    String s;
                    List<String> ing = new ArrayList<>();
                    shake.setIdName(singleShake.getKey());
                    shake.setLink(singleShake.child("link").getValue(String.class));
                    shake.setImage(singleShake.child("image").getValue(String.class));
                    shake.setName(singleShake.child("name").getValue(String.class));
                    shake.setId(singleShake.getKey());
                    //String x = singleShake.getKey();
                    /*if(Long.parseLong(x)){
                        shake.setIdStringNew(singleShake.child("id").getValue(String.class));
                    }else{
                        shake.setId(singleShake.child("id").getValue(Long.class));
                    }*/
                    //shake.setId(x);
                    shake.setPreparing(singleShake.child("preparing").getValue(String.class));
                    s =  singleShake.child("ingredients").getValue(String.class);
                    for (String split:s.split("\\|")){
                        if(split.trim()!=null && !split.isEmpty()){
                            ing.add(split);
                        }
                    }
                    shake.setIngredients(ing.toString());
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            realm.copyToRealmOrUpdate(shake);
                        }
                    });
                    /*milkshakes.add(milk);*/
                }
                prova();
                   /* shake.setLink(singleShake.child("image").getValue(String.class));
                    shake.setName(singleShake.child("name").getValue(String.class));
                    s =  singleShake.child("ingredients").getValue(String.class);
                    for (String split:s.split("\\|")){
                        if(split.trim()!=null && !split.isEmpty()){
                            ing.add(split);
                        }
                    }
                    shake.setIngredients(ing.toString());
                    shake.setPreparing(singleShake.child("preparing").getValue(String.class));
                    shakes.add(shake);
                }
                if (adapter != null) {
                    adapter.swap(new ArrayList<>(shakes));
                }*/
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };
        myshakes.addListenerForSingleValueEvent(postListener);

       /* Realm realm = Realm.getDefaultInstance();
        RealmQuery<Shake> query = realm.where(Shake.class);
        RealmResults<Shake> result1 = query.findAll();*/

        adapter = new ShakesAdapter(this.getContext());
       /* adapter.setData(result1);*/
        this.listview.setAdapter(adapter);
        prova();
        return rootView;
        /*adapter = new ShakesAdapter(this.getContext());
        adapter.setData(shakes);
        this.listview.setAdapter(adapter);*/
        /*adapter = new ShakesAdapter(this.getContext());
        this.listview.setAdapter(adapter);
        prova();
        return rootView;*/

    }

    private void prova(){

        Realm realm = Realm.getDefaultInstance();
        RealmQuery<Shake> query = realm.where(Shake.class);
        RealmResults<Shake> result1 = query.findAll();
        adapter.swap(new ArrayList<>(result1));
    }

    private class ShakesAdapter extends BaseAdapter{

        private LayoutInflater inflater;
        private List<Shake> shakes;

        public ShakesAdapter(final Context context){
            this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            shakes = new ArrayList<>();
        }

        public void setData(final List<Shake> shakes){
            this.shakes = shakes;
        }
        @Override
        public int getCount() {
            if(this.shakes == null){
                return 0;
            }
            return this.shakes.size();
        }

        @Override
        public Object getItem(int position) {
            if(this.shakes == null || this.shakes.get(position) == null){
                return null;
            }
            return this.shakes.get(position);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent){
            ViewHolder viewHolder;
            if(convertView == null){
                convertView = this.inflater.inflate(R.layout.shake_list_item, parent, false);
                viewHolder = new ViewHolder();
                viewHolder.initializeComponents(convertView);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            if(URLUtil.isValidUrl(shakes.get(position).getImage())){
                //Picasso
                Picasso.with(ShakesListFragment.this.getContext()).setLoggingEnabled(true);
                Picasso.with(ShakesListFragment.this.getContext()).load(shakes.get(position).getImage())
                        .resize(400,400).into(viewHolder.img);
            }else{

                byte[] decodedString = Base64.decode(shakes.get(position).getImage(),Base64.DEFAULT);
                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString,0,decodedString.length);

                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                decodedByte.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                String path = MediaStore.Images.Media.insertImage(ShakesListFragment.this.getContext().getContentResolver(),decodedByte, "title", null);

                Picasso.with(ShakesListFragment.this.getContext()).load(Uri.parse(path))
                        .resize(400,400).into(viewHolder.img);

                /*precedente
                byte[] decodedString = Base64.decode(shakes.get(position).getLink(),Base64.DEFAULT);
                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString,0,decodedString.length);

                Bitmap resized = Bitmap.createScaledBitmap(decodedByte, 400, 400, true);*/
                /*ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                decodedByte.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                String path = MediaStore.Images.Media.insertImage(ShakesListFragment.this.getContext().getContentResolver(),decodedByte, "title", null);
                String url = String.valueOf(Uri.parse(path));
                Picasso.with(ShakesListFragment.this.getContext()).load(url)
                        .resize(400,400).into(viewHolder.img);*/
               /*viewHolder.img.setImageBitmap(resized);*/
            }
            //Picasso
            viewHolder.stringImg.setText(shakes.get(position).getName());
            viewHolder.stringIngre.append(shakes.get(position).getIngredients());
            return convertView;

        }

        public void swap(List<Shake> newShakes) {
            this.shakes.clear();
            this.shakes.addAll(newShakes);
            notifyDataSetChanged();
        }
    }

    private static class ViewHolder{
        ImageView img;
        TextView stringImg;
        TextView stringIngre;
        public void initializeComponents(final View view){
            this.img = (ImageView) view.findViewById(R.id.img_list);
            this.stringImg =  (TextView) view.findViewById(R.id.title_shake);
            this.stringIngre = (TextView) view.findViewById(R.id.ingredients);
            stringIngre.setSelected(true);
        }
    }
}
