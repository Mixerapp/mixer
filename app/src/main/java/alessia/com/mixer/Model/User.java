package alessia.com.mixer.Model;

import android.provider.ContactsContract;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;


/**
 * Created by Alessia on 17/05/2017.
 */

public class User extends RealmObject {
    @PrimaryKey
    private String email;
    private String name;
    private String surname;
    private String password;
    private String confirmpass;


    public String getEmail() {
        return email;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getPassword() {
        return password;
    }

    public String getConfirmpass() {
        return confirmpass;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setConfirmpass(String confirmpass) {
        this.confirmpass = confirmpass;
    }


}
