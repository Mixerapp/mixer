package alessia.com.mixer;

import android.app.Application;
import android.content.res.Configuration;
import android.support.multidex.MultiDexApplication;

import com.crashlytics.android.Crashlytics;
import com.twitter.sdk.android.core.Twitter;

import io.fabric.sdk.android.Fabric;
import io.realm.Realm;

/**
 * Created by Alessia on 23/08/2017.
 */

public class MixerApplication extends MultiDexApplication {

    @Override
    public void onCreate() {
        super.onCreate();
        Twitter.initialize(this);
        Realm.init(this);
        Fabric.with(this, new Crashlytics());
        // Required initialization logic here!
    }

    // Called by the system when the device configuration changes while your component is running.
    // Overriding this method is totally optional!
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    // This is called when the overall system is running low on memory,
    // and would like actively running processes to tighten their belts.
    // Overriding this method is totally optional!
    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }
}
