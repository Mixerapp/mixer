package alessia.com.mixer.Activity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import alessia.com.mixer.Model.Milkshakes;
import alessia.com.mixer.Model.Shake;
import alessia.com.mixer.Model.Smoothies;
import alessia.com.mixer.R;
import alessia.com.mixer.databinding.InsertNewShakeBinding;

import static alessia.com.mixer.Activity.HomeActivity.REQUEST_IMAGE_CAPTURE;
import static alessia.com.mixer.Activity.HomeActivity.RESULT_LOAD_IMAGE;


/**
 * Created by Alessia on 29/08/2017.
 */

public class InsertActivity extends AppCompatActivity implements View.OnClickListener{
    private InsertNewShakeBinding binding;
    private String name, category, value_nu, ingrediets, preparing, link, image1, image2;
    private boolean favourite=false;
    private FirebaseDatabase database;
    private DatabaseReference category_database;
    static final int REQUEST_IMAGE_CAPTURE_INSERT=1;
    static final int RESULT_LOAD_IMAGE_INSERT=2;
    private boolean img1=false,img2=false,insertimg1=false,insertimg2=false;
    private boolean newshake=false,newsmoothie=false,newmilk=false;
    private Shake shake;
    private Smoothies smoothie;
    private Milkshakes milkshake;

    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        this.binding =  DataBindingUtil.setContentView(this, R.layout.insert_new_shake);

        TextView title = (TextView)this.binding.toolbar.findViewById(R.id.title);
        title.setText("INSERT");

        List<String> categories = new ArrayList<>();
        categories.add("shakes");
        categories.add("milkshakes");
        categories.add("smoothies");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,categories);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        this.binding.insCategory.setAdapter(dataAdapter);

        database = FirebaseDatabase.getInstance();

        this.binding.butInsert.setOnClickListener(this);
        this.binding.imgInsert1.setOnClickListener(this);
        this.binding.imgInsert2.setOnClickListener(this);

    }


    private void writeNewSmoothie(Smoothies smoothie) {
        HashMap<String,Object> result = new HashMap<>();
        smoothie.setName(name);
        smoothie.setIngredients(ingrediets);
        smoothie.setPreparing(preparing);
        smoothie.setFavorite(favourite);
        String newSmoothieID= category_database.push().getKey();
        result.put("id", newSmoothieID);
        result.put("ingredients",smoothie.getIngredients());
        result.put("name",smoothie.getName());
        result.put("preparing",smoothie.getPreparing());
        result.put("favourite",smoothie.isFavorite());
        if(insertimg1){
            smoothie.setImage(image1);
            result.put("image",smoothie.getImage());
            insertimg1=false;
        }else{
            result.put("image",null);
        }
        if(insertimg2){
            smoothie.setImages(image2);
            result.put("images",smoothie.getImages());
            insertimg2=false;
        }else{
            result.put("images",null);
        }
        if(value_nu!=null && !value_nu.isEmpty()){
            smoothie.setNutritionalValue(value_nu);
            result.put("nutritional_value",smoothie.getNutritionalValue());
        }
        if(link!=null && !link.isEmpty()){
            smoothie.setLink(link);
            result.put("link",smoothie.getLink());
        }
        category_database.child(newSmoothieID).setValue(result);
        startActivity(new Intent(this,HomeActivity.class));

    }

    private void writeNewMilk(Milkshakes milkshake) {
        HashMap<String,Object> result = new HashMap<>();
        milkshake.setName(name);
        milkshake.setIngredients(ingrediets);
        milkshake.setPreparing(preparing);
        milkshake.setFavorite(favourite);
        String newMilkshakeID= category_database.push().getKey();
        result.put("id", newMilkshakeID);
        result.put("ingredients",milkshake.getIngredients());
        result.put("name",milkshake.getName());
        result.put("preparing",milkshake.getPreparing());
        result.put("favourite",milkshake.isFavorite());
        if(insertimg1){
            milkshake.setImage(image1);
            result.put("image",milkshake.getImage());
            insertimg1=false;
        }else{
            result.put("image",null);
        }
        if(insertimg2){
            milkshake.setImages(image2);
            result.put("images",milkshake.getImages());
            insertimg2=false;
        }else{
            result.put("images",null);
        }
        if(value_nu!=null && !value_nu.isEmpty()){
            milkshake.setNutritionalValue(value_nu);
            result.put("nutritional_value",milkshake.getNutritionalValue());
        }
        if(link!=null && !link.isEmpty()){
            milkshake.setLink(link);
            result.put("link",milkshake.getLink());
        }
        category_database.child(newMilkshakeID).setValue(result);
        startActivity(new Intent(this,HomeActivity.class));
    }

    private void writeNewShake(Shake shake) {
        HashMap<String,Object> result = new HashMap<>();
        shake.setName(name);
        shake.setIngredients(ingrediets);
        shake.setPreparing(preparing);
        shake.setFavorite(favourite);
        String newShakeID= category_database.push().getKey();
        result.put("id", newShakeID);
        result.put("ingredients",shake.getIngredients());
        result.put("name",shake.getName());
        result.put("preparing",shake.getPreparing());
        result.put("favourite",shake.isFavorite());
        if(insertimg1){
            shake.setImage(image1);
            result.put("image",shake.getImage());
            insertimg1=false;
        }else{
            result.put("image",null);
        }
        if(insertimg2){
            shake.setImages(image2);
            result.put("images",shake.getImages());
            insertimg2=false;
        }else{
            result.put("images",null);
        }
        if(value_nu!=null && !value_nu.isEmpty()){
            shake.setNutritionalValue(value_nu);
            result.put("nutritional_value",shake.getNutritionalValue());
        }
        if(link!=null && !link.isEmpty()){
            shake.setLink(link);
            result.put("link",shake.getLink());
        }
        category_database.child(newShakeID).setValue(result);
        startActivity(new Intent(this,HomeActivity.class));
    }


       @Override
    public void onClick(View v) {
           switch (v.getId()){
               case R.id.but_insert:
                   switch (this.binding.insCategory.getSelectedItemPosition()){
                       case 0:
                           shake = new Shake();
                           newshake=true;
                           break;
                       case 1:
                           milkshake = new Milkshakes();
                           newmilk=true;
                           break;
                       case 2:
                           smoothie = new Smoothies();
                           newsmoothie=true;
                           break;
                   }
                   name=this.binding.insName.getText().toString().trim();
                   ingrediets=this.binding.insIng.getText().toString().trim();
                   preparing=this.binding.insPrep.getText().toString().trim();
                   value_nu=this.binding.insVal.getText().toString().trim();
                   link=this.binding.insLink.getText().toString().trim();
                   category = this.binding.insCategory.getSelectedItem().toString();
                   category_database = database.getReference(category);
                   if(name!=null && ingrediets!=null && preparing!=null &&!name.isEmpty() && !ingrediets.isEmpty() && !preparing.isEmpty()){
                       if(newshake){
                           writeNewShake(shake);
                       }
                       if(newsmoothie){
                           writeNewSmoothie(smoothie);
                       }
                       if(newmilk){
                           writeNewMilk(milkshake);
                       }
                   }else{
                       onCreateDialog("Inserisci tutti i campi obbligatori * !");
                   }
                   break;
               case R.id.img_insert1:
                   img1=true;
                   onCreateDialogforImage("Foto");
                   break;
               case R.id.img_insert2:
                   img2=true;
                   onCreateDialogforImage("Foto");
                   break;

       }

    }

    private AlertDialog onCreateDialog(String errore) {
        AlertDialog.Builder builder = new AlertDialog.Builder(InsertActivity.this);
        builder.setMessage(errore)
                .setTitle("Errore")
                .setCancelable(true)
                .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                })
                .show();
        return builder.create();
    }

    public Dialog onCreateDialogforImage (String error){
        final CharSequence opzioni[] = new CharSequence[] {"Scatta un foto", "Scegli una foto"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setItems(opzioni, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case 0:
                        dispatchTakePictureIntent();
                        break;
                    case 1:
                        Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(galleryIntent, RESULT_LOAD_IMAGE_INSERT);
                        break;
                }
            }
        });
        builder.show();

        return builder.create();
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE_INSERT);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            if(img1){
                ImageView img = (ImageView)this.binding.imgInsert1.findViewById(R.id.img_insert1);
                img.setImageBitmap(imageBitmap);
                img1=false;
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                imageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                byte[] b = baos.toByteArray();
                image1 = Base64.encodeToString(b, Base64.DEFAULT);
                insertimg1=true;

            }
            if(img2){
                ImageView img = (ImageView)this.binding.imgInsert2.findViewById(R.id.img_insert2);
                img.setImageBitmap(imageBitmap);
                img2=false;
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                imageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                byte[] b = baos.toByteArray();
                image2 = Base64.encodeToString(b, Base64.DEFAULT);
                insertimg2=true;
            }
        }
        if(requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK){
            Uri select = data.getData();
            if(img1){
                ImageView imgG = (ImageView )this.binding.imgInsert1.findViewById(R.id.img_insert1);
                imgG.setImageURI(select);
                img1=false;
                Bitmap bm = null;
                try {
                    bm = MediaStore.Images.Media.getBitmap(this.getContentResolver(),select);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bm.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                byte[] b = baos.toByteArray();
                image1 = Base64.encodeToString(b, Base64.DEFAULT);
                insertimg1=true;
            }
            if(img2){
                ImageView imgG = (ImageView )this.binding.imgInsert2.findViewById(R.id.img_insert2);
                imgG.setImageURI(select);
                img2=false;
                Bitmap bm = null;
                try {
                    bm = MediaStore.Images.Media.getBitmap(this.getContentResolver(),select);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bm.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                byte[] b = baos.toByteArray();
                image2 = Base64.encodeToString(b, Base64.DEFAULT);
                insertimg2=true;
            }
        }
    }
}
