package alessia.com.mixer.Activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareButton;
import com.facebook.share.widget.ShareDialog;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import alessia.com.mixer.Model.Milkshakes;
import alessia.com.mixer.R;
import alessia.com.mixer.databinding.ItemDetailsBinding;
import io.realm.Realm;

/**
 * Created by Alessia on 08/08/2017.
 */

public class ItemDetailsMilkShakes extends AppCompatActivity implements View.OnClickListener{
    private ItemDetailsBinding binding;
    private ImageView imageView;
    private TextView textTitle,textIngredients, textPrep;
    private String id;
    private String idName, linkforcondivision;
    private FirebaseDatabase database;
    private DatabaseReference milk_database;
    private DatabaseReference select_milk;
    private boolean fav=false;
    private CallbackManager callbackManager;
    private ShareDialog shareDialog;
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.binding = DataBindingUtil.setContentView(this, R.layout.item_details);
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();

        shareDialog = new ShareDialog(this);

        TextView title = (TextView) this.binding.toolbar.findViewById(R.id.title);
        title.setText("DETTAGLIO");

        database = FirebaseDatabase.getInstance();
        milk_database = database.getReference("milkshakes");
        // get intent data

        Realm realm = Realm.getDefaultInstance();


        Long MilkId = getIntent().getLongExtra("id",0);

        String Milkidstring = getIntent().getStringExtra("id");
        Milkshakes selectMilk = realm.where(Milkshakes.class).equalTo("id",Milkidstring).findFirst();

        // Selected image id
        //int position = i.getExtras().getInt("id");
        //Milkshakes selectMilk = (Milkshakes) i.getExtras().getSerializable("id");
        /*int idprova = i.getExtras().getInt("id");
        Log.d("idmilk",""+idprova);*/
        //ImageAdapter imageAdapter = new ImageAdapter(this);

        imageView = (ImageView) this.binding.imgItem.findViewById(R.id.img_item);
        textTitle = (TextView) this.binding.textTitle.findViewById(R.id.text_title);
        textIngredients = (TextView) this.binding.textIngredients.findViewById(R.id.text_Ingredients);
        textPrep = (TextView) this.binding.textPrep.findViewById(R.id.text_Prep);

        textTitle.setText(selectMilk.getName());

        String s;
        List<String> ing = new ArrayList<>();
        s =  selectMilk.getIngredients();
        id = selectMilk.getId();
        idName = selectMilk.getIdName();

        linkforcondivision = selectMilk.getImage();

        select_milk = milk_database.child(idName);
        for (String split:s.split("\\|")){
            if(split.trim()!=null && !split.isEmpty()){
                ing.add(split);
            }
        }
        textIngredients.setText(ing.toString());
        textPrep.setText(selectMilk.getPreparing());

        if(selectMilk.getImage()==null){

        }else{
            if(URLUtil.isValidUrl(selectMilk.getImage())){
                //Picasso
                Picasso.with(this.getApplicationContext()).setLoggingEnabled(true);
                Picasso.with(this.getApplicationContext()).load(selectMilk.getImage())
                        .resize(400,400).into(imageView);
            }else{

                byte[] decodedString = Base64.decode(selectMilk.getImage(),Base64.DEFAULT);
                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString,0,decodedString.length);

                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                decodedByte.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                String path = MediaStore.Images.Media.insertImage(this.getApplicationContext().getContentResolver(),decodedByte, "title", null);

                Picasso.with(this.getApplicationContext()).load(Uri.parse(path))
                        .resize(400,400).into(imageView);
            }
        }
        this.binding.checkFavourite.setOnClickListener(this);
        this.binding.condividi.setOnClickListener(this);
    }

    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }
    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.check_favourite:
                fav=true;
                this.binding.checkFavourite.setImageResource(R.drawable.favorite_black);
                select_milk.child("favourite").setValue(fav);
                break;
            case R.id.condividi:
                ShareLinkContent linkContent = new ShareLinkContent.Builder()
                        .setContentUrl(Uri.parse(linkforcondivision))
                        .build();
                shareDialog.show(linkContent);
        }
    }


}

