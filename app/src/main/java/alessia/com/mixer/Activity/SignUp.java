package alessia.com.mixer.Activity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;

import java.util.ArrayList;
import java.util.List;

import alessia.com.mixer.Model.User;
import alessia.com.mixer.R;
import alessia.com.mixer.databinding.SignUpBinding;
import at.favre.lib.dali.Dali;
import at.favre.lib.dali.builder.animation.BlurKeyFrameTransitionAnimation;

import static android.R.id.edit;

/**
 * Created by Alessia on 17/05/2017.
 */

public class SignUp extends AppCompatActivity implements View.OnClickListener {
    private SignUpBinding binding;
    private FirebaseAuth mAuth;
    private static final String TAG = "EmailPassword";

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.binding = DataBindingUtil.setContentView(this, R.layout.sign_up);

        TextView title = (TextView)this.binding.toolbar.findViewById(R.id.title);
        title.setText("SIGN UP");

        EditText pass = this.binding.editPass;
        pass.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        this.binding.butSignup.setOnClickListener(this);
        mAuth = FirebaseAuth.getInstance();
    }
    @Override
    public void onStart() {
        super.onStart();

        FirebaseUser currentUser = mAuth.getCurrentUser();
        //updateUI(currentUser);
    }
    private void createAccount(String email, String password) {
        Log.d(TAG, "createAccount:" + email);
        if (!validateForm()) {
            return;
        }

        //showProgressDialog();

        // [START create_user_with_email]
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {

                            Log.d(TAG, "createUserWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            //updateUI(user);
                        } else {

                            Log.w(TAG, "createUserWithEmail:failure", task.getException());
                            Toast.makeText(SignUp.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                            //updateUI(null);
                        }

                        // [START_EXCLUDE]
                       // hideProgressDialog();
                        // [END_EXCLUDE]
                    }
                });

        // [END create_user_with_email]
    }

    private boolean validateForm() {
        boolean valid = true;

        String email = this.binding.editEmail.getText().toString();
        if (TextUtils.isEmpty(email)) {
            this.binding.editEmail.setError("Required.");
            valid = false;
        } else {
            this.binding.editEmail.setError(null);
        }

        String password = this.binding.editPass.getText().toString();
        if (TextUtils.isEmpty(password)) {
            this.binding.editPass.setError("Required.");
            valid = false;
        } else {
            this.binding.editPass.setError(null);
        }

        return valid;
    }

    //aggiornamento interfaccia utente
    /*private void updateUI(FirebaseUser user) {
        //hideProgressDialog();
        if (user != null) {
            /*mStatusTextView.setText(getString(R.string.emailpassword_status_fmt,
                    user.getEmail(), user.isEmailVerified()));
            mDetailTextView.setText(getString(R.string.firebase_status_fmt, user.getUid()));*/

            /*findViewById(R.id.email_password_buttons).setVisibility(View.GONE);
            findViewById(R.id.email_password_fields).setVisibility(View.GONE);
            findViewById(R.id.signed_in_buttons).setVisibility(View.VISIBLE);*/

          // this.binding.butSignup.setEnabled(!user.isEmailVerified());
       /* } else {
            /*mStatusTextView.setText(R.string.signed_out);
            mDetailTextView.setText(null);

            findViewById(R.id.email_password_buttons).setVisibility(View.VISIBLE);
            findViewById(R.id.email_password_fields).setVisibility(View.VISIBLE);
            findViewById(R.id.signed_in_buttons).setVisibility(View.GONE);*/
      /*  }
    }*/



    public Dialog onCreateDialog(String error){
        AlertDialog.Builder builder = new AlertDialog.Builder(SignUp.this);
                    builder.setMessage(error)
                            .setTitle("Errore")
                            .setCancelable(true)
                            .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                        }
                    })
                        .show();



        return builder.create();
    }
    public void onClick(View v){
        switch (v.getId()){
            case R.id.but_signup:
                List<User> users = new ArrayList<>();
                User user = new User();
                user.setName(this.binding.editName.getText().toString());
                user.setSurname(this.binding.editSurname.getText().toString());
                user.setPassword(this.binding.editPass.getText().toString());
                user.setEmail(this.binding.editEmail.getText().toString());
                user.setConfirmpass(this.binding.editConfirmpass.getText().toString());
                String name = this.binding.editName.getText().toString();
                String surname = this.binding.editSurname.getText().toString();
                String pass = this.binding.editPass.getText().toString();
                String confpass = this.binding.editConfirmpass.getText().toString();
                String email = this.binding.editEmail.getText().toString();
                String dati = name + "||" +surname;

                if(name!=null && surname!=null && pass!=null && confpass!=null && email!=null && !name.isEmpty() && !surname.isEmpty() && !pass.isEmpty() && !confpass.isEmpty() && !email.isEmpty() ){

                    if (pass.equals(confpass) && pass.length()>=6){
                        users.add(user);
                        createAccount(user.getEmail().toString(),user.getPassword().toString());
                        FirebaseUser userFirebase = FirebaseAuth.getInstance().getCurrentUser();

                        UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                                .setDisplayName(dati)
                                .build();

                        userFirebase.updateProfile(profileUpdates)
                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (task.isSuccessful()) {
                                            Log.d(TAG, "User profile updated.");
                                        }
                                    }
                                });
                        startActivity(new Intent(this, Login.class));
                        break;
                    }else{
                        onCreateDialog("Password non corrette! (5caratteri almeno)");
                    }

                }else{
                    onCreateDialog("Alcuni campi non sono stati inseriti!");
                }

        }
    }
}
