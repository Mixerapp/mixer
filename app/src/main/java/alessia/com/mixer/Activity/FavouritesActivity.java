package alessia.com.mixer.Activity;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import alessia.com.mixer.Model.Favourites;
import alessia.com.mixer.Model.Milkshakes;
import alessia.com.mixer.R;
import alessia.com.mixer.databinding.PreferesListBinding;


/**
 * Created by Alessia on 20/09/2017.
 */

public class FavouritesActivity extends AppCompatActivity {
    private PreferesListBinding binding;
    private FirebaseDatabase database;
    private DatabaseReference milk,shake,smoothies, prova;
    private FavouriteAdapter adapter;
    private List<Favourites> favourites = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.binding = DataBindingUtil.setContentView(this, R.layout.preferes_list);

        database = FirebaseDatabase.getInstance();

        TextView title = (TextView) this.binding.toolbar.findViewById(R.id.title);
        title.setText("PREFERITI");

        prova = database.getReference("mixer-41f8b");
        milk = database.getReference("milkshakes");
        shake = database.getReference("shakes");
        smoothies = database.getReference("smoothies");

        ValueEventListener postListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot singleMilk : dataSnapshot.getChildren()) {
                    Favourites milkFav = new Favourites();
                    Log.d("DENTRO",""+singleMilk.child("favourite").getValue(Boolean.class));
                    if(singleMilk.child("favourite").getValue(Boolean.class)){
                        milkFav.setName(singleMilk.child("name").getValue(String.class));
                        milkFav.setImage(singleMilk.child("image").getValue(String.class));
                        Log.d("DENTRO","2");
                        favourites.add(milkFav);
                    }

                }
                if (adapter != null) {
                    adapter.swap(new ArrayList<>(favourites));
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        };
        milk.addListenerForSingleValueEvent(postListener);

        ValueEventListener postListener1 = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot singleShake : dataSnapshot.getChildren()) {
                    Favourites shakeFav = new Favourites();
                    Log.d("DENTRO3",""+singleShake.child("favourite").getValue(Boolean.class));
                    if(singleShake.child("favourite").getValue(Boolean.class)){
                        Log.d("DENTRO","3");
                        shakeFav.setName(singleShake.child("name").getValue(String.class));
                        shakeFav.setImage(singleShake.child("image").getValue(String.class));
                        favourites.add(shakeFav);
                    }

                }
                if (adapter != null) {
                    adapter.swap(new ArrayList<>(favourites));
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        };
        shake.addListenerForSingleValueEvent(postListener1);

        ValueEventListener postListener2 = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot singleSmoothie : dataSnapshot.getChildren()) {
                    Favourites smoothieFav = new Favourites();
                    if(singleSmoothie.child("favourite").getValue(Boolean.class)){
                        smoothieFav.setName(singleSmoothie.child("name").getValue(String.class));
                        smoothieFav.setImage(singleSmoothie.child("image").getValue(String.class));
                        favourites.add(smoothieFav);
                    }

                }
                if (adapter != null) {
                    adapter.swap(new ArrayList<>(favourites));
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        };
        smoothies.addListenerForSingleValueEvent(postListener2);

        adapter = new FavouriteAdapter(this.getApplicationContext());
        adapter.setData(favourites);
        this.binding.preferesGridList.setAdapter(adapter);

    }
    private class FavouriteAdapter extends BaseAdapter {

        private LayoutInflater inflater;
        private List<Favourites> favourites;

        public FavouriteAdapter(final Context context){
            this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        public void setData(final List<Favourites> favourites){
            this.favourites = favourites;
        }
        @Override
        public int getCount() {
            if(this.favourites== null){
                return 0;
            }
            return this.favourites.size();
        }

        @Override
        public Object getItem(int position) {
            if(this.favourites == null || this.favourites.get(position) == null){
                return null;
            }
            return this.favourites.get(position);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent){
            ViewHolder viewHolder;
            if(convertView == null){
                convertView = this.inflater.inflate(R.layout.preferes_list_item, parent, false);
                viewHolder = new ViewHolder();
                viewHolder.initializeComponents(convertView);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            if(URLUtil.isValidUrl(favourites.get(position).getImage())){
                //Picasso
                Picasso.with(FavouritesActivity.this).setLoggingEnabled(true);
                Picasso.with(FavouritesActivity.this).load(favourites.get(position).getImage())
                        .resize(400,400).into(viewHolder.img);
            }else{

                byte[] decodedString = Base64.decode(favourites.get(position).getImage(),Base64.DEFAULT);
                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString,0,decodedString.length);

                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                decodedByte.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                String path = MediaStore.Images.Media.insertImage(FavouritesActivity.this.getContentResolver(),decodedByte, "title", null);

                Picasso.with(FavouritesActivity.this).load(Uri.parse(path))
                        .resize(400,400).into(viewHolder.img);
            }

            viewHolder.stringImg.setText(favourites.get(position).getName());
            return convertView;
        }

        public void swap(List<Favourites> newFav) {
            this.favourites.clear();
            this.favourites.addAll(newFav);
            notifyDataSetChanged();
        }
    }

    private static class ViewHolder{
        ImageView img;
        TextView stringImg;
        public void initializeComponents(final View view){
            this.img = (ImageView) view.findViewById(R.id.preferes_img_list);
            this.stringImg =  (TextView) view.findViewById(R.id.title_preferes_shake);
        }
    }

}
