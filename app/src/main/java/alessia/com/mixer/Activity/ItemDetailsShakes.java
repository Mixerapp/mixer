package alessia.com.mixer.Activity;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.net.wifi.p2p.WifiP2pManager;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.google.android.gms.common.images.ImageManager;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Observer;

import alessia.com.mixer.Fragment.ShakesListFragment;
import alessia.com.mixer.Model.Shake;
import alessia.com.mixer.R;
import alessia.com.mixer.databinding.ItemDetailsBinding;
import io.realm.Realm;

import static alessia.com.mixer.R.drawable.shakes;

/**
 * Created by Alessia on 18/05/2017.
 */

public class ItemDetailsShakes extends AppCompatActivity implements View.OnClickListener{

    private ItemDetailsBinding binding;
    private ImageView imageView;
    private TextView textTitle,textIngredients, textPrep;
    private String idName, linkforcondivision;
    private FirebaseDatabase database;
    private DatabaseReference shake_database;
    private DatabaseReference select_shake;
    private boolean fav=false;
    private CallbackManager callbackManager;
    private ShareDialog shareDialog;
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.binding = DataBindingUtil.setContentView(this, R.layout.item_details);
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();

        shareDialog = new ShareDialog(this);

        TextView title = (TextView) this.binding.toolbar.findViewById(R.id.title);
        title.setText("DETTAGLIO");

        database = FirebaseDatabase.getInstance();
        shake_database = database.getReference("shakes");

        Realm realm = Realm.getDefaultInstance();

        String Shakeidstring = getIntent().getStringExtra("id");
        Shake selectShake = realm.where(Shake.class).equalTo("id",Shakeidstring).findFirst();
        // get intent data
       // Intent i = getIntent();

        // Selected image id
        //int position = i.getExtras().getInt("id");
        //Shake selectShake = (Shake) i.getExtras().getSerializable("id");
        //List<Shake> shakes = new ArrayList<>();
        //ImageAdapter imageAdapter = new ImageAdapter(this);

        imageView = (ImageView) this.binding.imgItem.findViewById(R.id.img_item);
        textTitle = (TextView) this.binding.textTitle.findViewById(R.id.text_title);
        textIngredients = (TextView) this.binding.textIngredients.findViewById(R.id.text_Ingredients);
        textPrep = (TextView) this.binding.textPrep.findViewById(R.id.text_Prep);

        textTitle.setText(selectShake.getName());

        String s;
        List<String> ing = new ArrayList<>();

        idName = selectShake.getIdName();

        linkforcondivision = selectShake.getImage();

        select_shake = shake_database.child(idName);
        s =  selectShake.getIngredients();
        for (String split:s.split("\\|")){
            if(split.trim()!=null && !split.isEmpty()){
                ing.add(split);
            }
        }
        textIngredients.setText(ing.toString());
        textPrep.setText(selectShake.getPreparing());

        if(selectShake.getImage()==null){

        }else{
            if(URLUtil.isValidUrl(selectShake.getImage())){
                //Picasso
                Picasso.with(this.getApplicationContext()).setLoggingEnabled(true);
                Picasso.with(this.getApplicationContext()).load(selectShake.getImage())
                        .resize(400,400).into(imageView);
            }else{

                byte[] decodedString = Base64.decode(selectShake.getLink(),Base64.DEFAULT);
                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString,0,decodedString.length);

                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                decodedByte.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                String path = MediaStore.Images.Media.insertImage(this.getApplicationContext().getContentResolver(),decodedByte, "title", null);

                Picasso.with(this.getApplicationContext()).load(Uri.parse(path))
                        .resize(400,400).into(imageView);
            }
        }
        this.binding.checkFavourite.setOnClickListener(this);
        this.binding.condividi.setOnClickListener(this);

    }

    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    public void onClick(View v) {

        switch (v.getId()){
            case R.id.check_favourite:
                fav=true;
                this.binding.checkFavourite.setImageResource(R.drawable.favorite_black);
                select_shake.child("favourite").setValue(fav);
                break;
            case R.id.condividi:
                ShareLinkContent linkContent = new ShareLinkContent.Builder()
                        .setContentUrl(Uri.parse(linkforcondivision))
                        .build();
                shareDialog.show(linkContent);
        }
    }

}
