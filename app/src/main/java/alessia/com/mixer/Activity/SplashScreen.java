package alessia.com.mixer.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import alessia.com.mixer.R;
import alessia.com.mixer.Utils.Utils;

/**
 * Created by Alessia on 15/05/2017.
 */

public class SplashScreen extends AppCompatActivity {
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);
        Thread myth = new Thread(){
            public void run(){
                try{
                    sleep(3000);
                    Intent intent;
                    Log.d("utilsuser","prova"+Utils.getUserId(SplashScreen.this));
                    if (Utils.getUserId(SplashScreen.this) == null) {
                        intent = new Intent(getApplicationContext(),Login.class);

                    }else{
                        intent = new Intent(getApplicationContext(),HomeActivity.class);
                    }

                    startActivity(intent);
                    /*startActivity(new Intent(getApplicationContext(),Login.class));*/
                    finish();

                }catch (InterruptedException e){
                    e.printStackTrace();
                }
            }
        };
            myth.start();
    }
}
