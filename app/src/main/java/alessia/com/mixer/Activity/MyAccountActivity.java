package alessia.com.mixer.Activity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserInfo;

import java.util.ArrayList;
import java.util.List;

import alessia.com.mixer.R;
import alessia.com.mixer.Utils.Utils;
import alessia.com.mixer.databinding.MyAccountBinding;


/**
 * Created by Alessia on 12/06/2017.
 */

public class MyAccountActivity extends AppCompatActivity implements View.OnClickListener{


    private MyAccountBinding binding;
    private String dati;
    private String email;
    private String pass;
    private String name;
    private String surname;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        this.binding =  DataBindingUtil.setContentView(this, R.layout.my_account);

        TextView title = (TextView)this.binding.toolbar.findViewById(R.id.title);
        title.setText("MY ACCOUNT");

        TextView passView = this.binding.setPass;
        passView.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            for (UserInfo profile : user.getProviderData()) {
                String providerId = profile.getProviderId();

                //providerId.compareTo("google")
                // UID specific to the provider
                String uid = profile.getUid();

                // Name, email address, and profile photo Url
                String name = profile.getDisplayName();
                String email = profile.getEmail();
                Uri photoUrl = profile.getPhotoUrl();
            };
        }
        if (user != null) {
           name = user.getDisplayName();
           /* ArrayList<String> list = new ArrayList<>();
            for(String split: dati.split("\\s")){
                list.add(split);

            }*/
            /*name=list.get(0);*/
            //surname=list.get(1);
            email = user.getEmail();

            boolean emailVerified = user.isEmailVerified();

            String uid = user.getUid();
        }

        this.binding.setName.setText(name);
        this.binding.setSurname.setText(surname);
        this.binding.setMail.setText(email);
        this.binding.changePass.setOnClickListener(this);
        this.binding.logout.setOnClickListener(this);

    }

    public Dialog onCreateDialog(String error){
        AlertDialog.Builder builder = new AlertDialog.Builder(MyAccountActivity.this);
        builder.setMessage(error)
                .setCancelable(true)
                .setPositiveButton("si", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        FirebaseAuth.getInstance().signOut();
                        LoginManager.getInstance().logOut();
                        Utils.setUserId(MyAccountActivity.this,null);
                        startActivity(new Intent(MyAccountActivity.this, Login.class));
                    }
                })

                .setNegativeButton("no", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                })
                .show();



        return builder.create();
    }
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.change_pass:
                break;
            case  R.id.logout:
                onCreateDialog("Sei sicuro di uscire?");
                break;
        }

    }
}
