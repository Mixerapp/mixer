package alessia.com.mixer.Activity;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Environment;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;
import org.xmlpull.v1.XmlPullParser;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import alessia.com.mixer.Fragment.HomeFragment;
import alessia.com.mixer.Fragment.MilkshakesListFragment;
import alessia.com.mixer.Fragment.ShakesListFragment;
import alessia.com.mixer.Fragment.SmoothiesListFragment;
import alessia.com.mixer.Model.Milkshakes;
import alessia.com.mixer.Model.News;
import alessia.com.mixer.Model.User;
import alessia.com.mixer.R;
import alessia.com.mixer.Utils.Utils;
import alessia.com.mixer.databinding.HomeBinding;
import alessia.com.mixer.databinding.NewsItemBinding;
import io.realm.Realm;
import io.realm.annotations.PrimaryKey;

import static android.R.attr.breadCrumbTitle;
import static android.R.attr.data;
import static android.R.attr.defaultHeight;
import static android.R.attr.fragment;
import static android.R.attr.permission;
import static android.R.attr.width;
import static android.provider.UserDictionary.Words.APP_ID;

public class HomeActivity extends AppCompatActivity implements View.OnClickListener,GoogleApiClient.ConnectionCallbacks,GoogleApiClient.OnConnectionFailedListener {

    private static final int PERMISSION_ACCESS_COARSE_LOCATION = 1;
    private GoogleApiClient googleApiClient;
    private HomeBinding binding;
    private Toolbar toolbar;
    private DrawerLayout menuLayout;
    private String name,dati,surname;
    private String cityName = "Not Found";
    private TextView cittàUser, nameUser;
    static final int REQUEST_IMAGE_CAPTURE = 1;
    static final int RESULT_LOAD_IMAGE = 2;
    String mCurrentPhotoPath;

    private String TAG = "prova";
    private ActionBarDrawerToggle drawerToggle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.binding = DataBindingUtil.setContentView(this, R.layout.home);

        if (ContextCompat.checkSelfPermission(HomeActivity.this,
                Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(HomeActivity.this,
                    Manifest.permission.ACCESS_COARSE_LOCATION)) {

                Snackbar.make(this.binding.menuLayout,
                        "Permission",
                        Snackbar.LENGTH_INDEFINITE)
                        .setAction(android.R.string.ok, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                // Request the permission again.
                                ActivityCompat.requestPermissions(HomeActivity.this,
                                        new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                                        PERMISSION_ACCESS_COARSE_LOCATION);
                            }
                        }).show();

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(HomeActivity.this,
                        new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                        PERMISSION_ACCESS_COARSE_LOCATION);
            }
        }
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            // Name, email address, and profile photo Url
            dati = user.getDisplayName();
            /*ArrayList<String> list = new ArrayList<>();
            for(String split: dati.split("\\|")){
                list.add(split);

            }
            name=list.get(0);*/
            //surname=list.get(1);
        }

        //Utils.setUserId(HomeActivity.this,user.getProviderId());
       googleApiClient = new GoogleApiClient.Builder(this ,this , this).addApi(LocationServices.API).build();

        TextView title = (TextView) this.binding.toolbar.findViewById(R.id.title);
        title.setText("HOME");

        cittàUser = (TextView) this.binding.drawer.header.findViewById(R.id.città);
        cittàUser.setText(cityName);
        nameUser = (TextView) this.binding.drawer.header.findViewById(R.id.nameuser);
        nameUser.setText(dati);
        toolbar = (Toolbar) this.binding.toolbar.findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        this.menuLayout = (DrawerLayout) this.binding.menuLayout.findViewById(R.id.menu_layout);
        drawerToggle = setupDrawerToggle();
        this.menuLayout.addDrawerListener(drawerToggle);

        this.switchFragments(R.id.home);

        this.binding.drawer.home.setOnClickListener(this);
        this.binding.drawer.shakes.setOnClickListener(this);
        this.binding.drawer.smoothies.setOnClickListener(this);
        this.binding.drawer.milkshakes.setOnClickListener(this);
        this.binding.drawer.favorites.setOnClickListener(this);
        this.binding.drawer.insertProduct.setOnClickListener(this);
        this.binding.drawer.account.setOnClickListener(this);
        this.binding.drawer.header.findViewById(R.id.circularImageView).setOnClickListener(this);
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            ImageView img = (ImageView )this.binding.drawer.header.findViewById(R.id.circularImageView);
            img.setImageBitmap(imageBitmap);
            UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                    .setPhotoUri(Uri.parse(String.valueOf(imageBitmap)))
                    .build();
            //Picasso.with(HomeActivity.this).load(String.valueOf(imageBitmap)).resize(400,400).into((ImageView)this.binding.drawer.header.findViewById(R.id.circularImageView));
        }
        if(requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && data!=null){
            Uri select = data.getData();
            ImageView imgG = (ImageView )this.binding.drawer.header.findViewById(R.id.circularImageView);
            imgG.setImageURI(select);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_ACCESS_COARSE_LOCATION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // All good!
                } else {
                    Toast.makeText(this, "Need your location!", Toast.LENGTH_SHORT).show();
                }

                break;
            case RESULT_LOAD_IMAGE:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(galleryIntent, RESULT_LOAD_IMAGE);
                } else {
                    //do something like displaying a message that he didn`t allow the app to access gallery and you wont be able to let him select from gallery
                }
                break;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (googleApiClient != null) {
            googleApiClient.connect();
        }
    }

    @Override
    public void onConnected(Bundle bundle) {

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            Location lastLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);

            double lat = lastLocation.getLatitude(), lon = lastLocation.getLongitude();
            /*String units = "imperial";
            String url = String.format("http://api.openweathermap.org/data/2.5/weather?lat=%f&lon=%f&units=%s&appid=%s",
                    lat, lon, units, APP_ID);*/
            ;
            cittàUser.setText(getLocationName(lat, lon));
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    private ActionBarDrawerToggle setupDrawerToggle() {
        // NOTE: Make sure you pass in a valid toolbar reference.  ActionBarDrawToggle() does not require it
        // and will not render the hamburger icon without it.
        return new ActionBarDrawerToggle(this, menuLayout, toolbar, R.string.drawer_open,  R.string.drawer_close);
    }
    /*private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        selectDrawerItem(menuItem);
                        return true;
                    }
                });
    }*/

    public String getLocationName(double latitude, double longitude) {

        Geocoder gcd = new Geocoder(this, Locale.getDefault());
        try {

            List<Address> addresses = gcd.getFromLocation(latitude, longitude, 1);

            cityName = addresses.get(0).getLocality();
            String stateName = addresses.get(0).getAddressLine(1);
            String countryName = addresses.get(0).getAddressLine(2);

           /* for (Address adrs : addresses) {
                if (adrs != null) {

                    String city = adrs.getLocality();
                    if (city != null && !city.equals("")) {
                        cityName = city;
                        System.out.println("city ::  " + cityName);
                    } else {

                    }
                    // // you should also try with addresses.get(0).toSring();

                }

            }*/
        } catch (IOException e) {
            e.printStackTrace();
        }
        return cityName;

    }

    public Dialog onCreateDialog (String error){
        final CharSequence opzioni[] = new CharSequence[] {"Scatta un foto", "Scegli una foto"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setItems(opzioni, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case 0:
                        dispatchTakePictureIntent();
                        break;
                    case 1:
                        Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(galleryIntent, RESULT_LOAD_IMAGE);
                        break;
                }
            }
        });
        builder.show();

        return builder.create();
    }


    private void switchFragments(int id){
        Fragment fragment = null;
        TextView title = (TextView) this.binding.toolbar.findViewById(R.id.title);
        switch (id) {
            case R.id.home:
                fragment = new HomeFragment();
                moveToFragment(fragment);
                break;
            case R.id.shakes:
                fragment = new ShakesListFragment();
                title.setText("SHAKE");
                moveToFragment(fragment);
                break;
            case R.id.smoothies:
                fragment = new SmoothiesListFragment();
                title.setText("SMOOTHIES");
                moveToFragment(fragment);
                break;
            case R.id.milkshakes:
                fragment = new MilkshakesListFragment();
                title.setText("MILKSHAKES");
                moveToFragment(fragment);
                break;
            case R.id.favorites:
                title.setText("FAVOURITE");
                startActivity(new Intent(this, FavouritesActivity.class));
                break;
            case R.id.insert_product:
                startActivity(new Intent(this,InsertActivity.class));
                break;
            case R.id.account:
                startActivity(new Intent(this, MyAccountActivity.class));
                break;
            case R.id.circularImageView:
                onCreateDialog("Foto");
                break;
        }

        this.binding.menuLayout.closeDrawers();
    }
    public void onClick(View v) {
        this.switchFragments(v.getId());

    }
    private void moveToFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, fragment, fragment.getClass().getSimpleName()).addToBackStack(null).commit();

        }


    /*@Override
   /* public boolean onOptionsItemSelected(MenuItem item) {
        // The action bar home/up action should open or close the drawer.
        switch (item.getItemId()) {
            case android.R.id.home:
                this.menuLayout.openDrawer(GravityCompat.START);
                return true;
        }


        return super.onOptionsItemSelected(item);
    }*/

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggles
        drawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onStop() {
        googleApiClient.disconnect();
        super.onStop();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.i(HomeActivity.class.getSimpleName(), "Can't connect to Google Play Services!");
    }

}

