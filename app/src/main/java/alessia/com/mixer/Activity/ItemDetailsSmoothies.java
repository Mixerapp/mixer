package alessia.com.mixer.Activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.webkit.URLUtil;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import alessia.com.mixer.Model.Smoothies;
import alessia.com.mixer.R;
import alessia.com.mixer.databinding.ItemDetailsBinding;

/**
 * Created by Alessia on 08/09/2017.
 */

public class ItemDetailsSmoothies extends AppCompatActivity {
    private ItemDetailsBinding binding;
    private ImageView imageView;
    private TextView textTitle,textIngredients, textPrep;
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        this.binding = DataBindingUtil.setContentView(this, R.layout.item_details);

        TextView title = (TextView) this.binding.toolbar.findViewById(R.id.title);
        title.setText("DETTAGLIO");

        Intent i = getIntent();

        // Selected image id
        //int position = i.getExtras().getInt("id");
         Smoothies selectSmoothies = (Smoothies) i.getExtras().getSerializable("id");
        //ImageAdapter imageAdapter = new ImageAdapter(this);

        imageView = (ImageView) this.binding.imgItem.findViewById(R.id.img_item);
        textTitle = (TextView) this.binding.textTitle.findViewById(R.id.text_title);
        textIngredients = (TextView) this.binding.textIngredients.findViewById(R.id.text_Ingredients);
        textPrep = (TextView) this.binding.textPrep.findViewById(R.id.text_Prep);

        textTitle.setText(selectSmoothies.getName());
        String s;
        List<String> ing = new ArrayList<>();
        s =  selectSmoothies.getIngredients();
        for (String split:s.split("\\|")){
            if(split.trim()!=null && !split.isEmpty()){
                ing.add(split);
            }
        }
        textIngredients.setText(ing.toString());
        textPrep.setText(selectSmoothies.getPreparing());

        if(selectSmoothies.getLink()==null){

        }else{
            if(URLUtil.isValidUrl(selectSmoothies.getLink())){
                //Picasso
                Picasso.with(this.getApplicationContext()).setLoggingEnabled(true);
                Picasso.with(this.getApplicationContext()).load(selectSmoothies.getLink())
                        .resize(400,400).into(imageView);
            }else{

                byte[] decodedString = Base64.decode(selectSmoothies.getLink(),Base64.DEFAULT);
                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString,0,decodedString.length);

                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                decodedByte.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                String path = MediaStore.Images.Media.insertImage(this.getApplicationContext().getContentResolver(),decodedByte, "title", null);

                Picasso.with(this.getApplicationContext()).load(Uri.parse(path))
                        .resize(400,400).into(imageView);
            }
        }
    }
}
