package alessia.com.mixer.Activity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.internal.CallbackManagerImpl;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.auth.TwitterAuthProvider;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.DefaultLogger;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterConfig;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;

import org.w3c.dom.Text;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import alessia.com.mixer.Model.User;
import alessia.com.mixer.R;
import alessia.com.mixer.Utils.Utils;
import alessia.com.mixer.databinding.LoginBinding;
import io.fabric.sdk.android.Fabric;


/**
 * Created by Alessia on 16/05/2017.
 */

public class Login extends AppCompatActivity implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener{

    private LoginBinding binding;
    private FirebaseAuth mAuth;
    private static final String TAG = "EmailPassword";
    private boolean authentication = false,authFace=false,authTwitter=false;
    private GoogleApiClient mGoogleApiClient;
    private CallbackManager mCallbackManager;
    private TwitterLoginButton mLoginButton;

    private static final String TAG1 = "GoogleActivity";
    private static final int RC_SIGN_IN = 9001;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
        Twitter.initialize(this);
        this.binding = DataBindingUtil.setContentView(this, R.layout.login);

        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "alessia.com.mixer",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }
        /*EditText pass = this.binding.editPass;
        pass.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);*/

        TwitterConfig config = new TwitterConfig.Builder(this)
                .logger(new DefaultLogger(Log.DEBUG))
                .twitterAuthConfig(new TwitterAuthConfig("CONSUMER_KEY", "CONSUMER_SECRET"))
                .debug(true)
                .build();
        Twitter.initialize(config);

        TextView title = (TextView)this.binding.toolbar.findViewById(R.id.title);
        title.setText("LOGIN");

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        mCallbackManager = CallbackManager.Factory.create();
        //LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile"));
        this.binding.butFacebook.setReadPermissions("email", "public_profile");
        this.binding.butFacebook.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d(TAG, "facebook:onSuccess:" + loginResult);
                if(authFace){
                    handleFacebookAccessToken(loginResult.getAccessToken());
                }
            }
            @Override
            public void onCancel() {
                Log.d(TAG, "facebook:onCancel");
                // [START_EXCLUDE]
                //updateUI(null);
                // [END_EXCLUDE]
            }
            @Override
            public void onError(FacebookException error) {
                Log.d(TAG, "facebook:onError", error);
                // [START_EXCLUDE]
               // updateUI(null);
                // [END_EXCLUDE]
            }
        });
        mLoginButton = this.binding.butTwitter;
        mLoginButton.setCallback(new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {
                Log.d(TAG, "twitterLogin:success" + result);
                if(authTwitter){
                    handleTwitterSession(result.data);
                }

            }
            @Override
            public void failure(TwitterException exception) {
                Log.w(TAG, "twitterLogin:failure", exception);
                //updateUI(null);
            }
        });
        this.binding.butLogin.setOnClickListener(this);
        this.binding.textSignup.setOnClickListener(this);
        this.binding.textForgot.setOnClickListener(this);
        this.binding.butFacebook.setOnClickListener(this);
        this.binding.butGoogle.setOnClickListener(this);
        this.binding.butTwitter.setOnClickListener(this);
        mAuth = FirebaseAuth.getInstance();
    }

    private void handleTwitterSession(TwitterSession session) {
        Log.d(TAG, "handleTwitterSession:" + session);

        AuthCredential credential = TwitterAuthProvider.getCredential(
                session.getAuthToken().token,
                session.getAuthToken().secret);

        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            startActivity(new Intent(Login.this,HomeActivity.class));
                            //updateUI(user);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            Toast.makeText(Login.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                            //updateUI(null);
                        }

                        // ...
                    }
                });
    }
    private void handleFacebookAccessToken(AccessToken token) {
        Log.d("utilsuser","prova"+Utils.getUserId(Login.this));
        Log.d(TAG, "handleFacebookAccessToken:" + token);
        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.d("utilsuser","prova"+Utils.getUserId(Login.this));
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            if (user != null) {
                                Utils.setUserId(Login.this, user.getUid());
                            }
                            Log.d("utilsuser","prova"+Utils.getUserId(Login.this));
                            authFace=false;
                            startActivity(new Intent(Login.this,HomeActivity.class));
                        } else {
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            Toast.makeText(Login.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }
    private void signInGoogle() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.d("utilsuser","prova"+Utils.getUserId(Login.this));
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (result.isSuccess()) {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = result.getSignInAccount();
                firebaseAuthWithGoogle(account);
            } else {
                onCreateDialog1("Autenticazione fallita");
            }
        }else if(FacebookSdk.isFacebookRequestCode(requestCode)){
            Log.d("utilsuser","prova"+Utils.getUserId(Login.this));
            if (requestCode == CallbackManagerImpl.RequestCodeOffset.Login.toRequestCode()) {
                mCallbackManager.onActivityResult(requestCode, resultCode, data);
            }
            else if (requestCode == CallbackManagerImpl.RequestCodeOffset.Share.toRequestCode()){
                //share
            }

        }else if(requestCode==TwitterAuthConfig.DEFAULT_AUTH_REQUEST_CODE){
            mLoginButton.onActivityResult(requestCode, resultCode, data);
        }

    }

    // [START auth_with_google]requestCode==TwitterAuthConfig.DEFAULT_AUTH_REQUEST_CODE
    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        Log.d(TAG, "firebaseAuthWithGoogle:" + acct.getId());
        // [START_EXCLUDE silent]
        //showProgressDialog();
        // [END_EXCLUDE]

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            if (user != null) {
                                Utils.setUserId(Login.this, user.getUid());
                            }
                            startActivity(new Intent(Login.this,HomeActivity.class));
                            //updateUI(user);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            Toast.makeText(Login.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                            //updateUI(null);
                        }

                        // [START_EXCLUDE]
                        //hideProgressDialog();
                        // [END_EXCLUDE]
                    }
                });
    }
    // [END auth_with_google]
    private void signIn(String email, String password) {
        Log.d(TAG, "signIn:" + email);

        // [START sign_in_with_email]
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        authentication=true;
                        if (task.isSuccessful()) {

                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            if (user != null) {
                                Utils.setUserId(Login.this, user.getUid());
                            }
                            startActivity(new Intent(Login.this, HomeActivity.class));

                            //updateUI(user);
                        } else {
                            authentication=false;
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithEmail:failure", task.getException());
                            /*Toast.makeText(Login.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();*/

                            onCreateDialog1("Autenticazione fallita!");
                        }
                    }
                });
    }

    public Dialog onCreateDialog1(String error){
        AlertDialog.Builder builder = new AlertDialog.Builder(Login.this);
        builder.setMessage(error)
                .setTitle("Errore")
                .setCancelable(true)
                .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                })
                .show();
        return builder.create();
    }

    public Dialog onCreateDialog2 (String error){
        final EditText edittext = new EditText(Login.this);
        final AlertDialog.Builder builder = new AlertDialog.Builder(Login.this);
        builder.setMessage("Inserire l'email");
        builder.setTitle("Attenzione");
        builder.setCancelable(true);
        builder.setView(edittext);
        builder.setPositiveButton("invia", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        FirebaseAuth auth = FirebaseAuth.getInstance();
                        String mail = edittext.getText().toString().trim();
                        auth.sendPasswordResetEmail(mail)
                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (task.isSuccessful()) {
                                            Log.d(TAG, "Email sent.");
                                            Toast toast = Toast.makeText(getApplicationContext(),"email Inviata",Toast.LENGTH_SHORT);
                                            toast.show();

                                        } else{
                                            Log.d(TAG, "Email not sent.");

                                        }
                                    }
                                });
                    }
                })
                .setNegativeButton("annulla", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                })
                .show();
        return builder.create();
    }

    public void onClick(View v){
        switch (v.getId()){
            case R.id.but_login:
                String user = this.binding.editUser.getText().toString().trim();
                String pass = this.binding.editPass.getText().toString().trim();
                if(user!= null && pass!=null && !user.isEmpty() && !pass.isEmpty()){
                    signIn(user, pass);
                }else{
                    onCreateDialog1("Campi vuoti");
                }
                break;
            case R.id.text_signup:
                startActivity(new Intent(this, SignUp.class));
                break;
            case R.id.text_forgot:
                onCreateDialog2("Password dimenticata?");
                break;
            case R.id.but_google:
                signInGoogle();
                break;
            case R.id.but_facebook:
                authFace=true;
                break;
            case R.id.but_twitter:
                authTwitter=true;
                break;
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
